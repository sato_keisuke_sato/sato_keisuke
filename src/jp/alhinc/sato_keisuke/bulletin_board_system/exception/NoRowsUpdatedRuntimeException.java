package jp.alhinc.sato_keisuke.bulletin_board_system.exception;


public class NoRowsUpdatedRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;
}

package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.FormUser;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Position;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Store;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;
import jp.alhinc.sato_keisuke.bulletin_board_system.exception.NoRowsUpdatedRuntimeException;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.PositionsService;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.StoresService;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.UserService;

/**
 * Servlet implementation class postController
 */
@WebServlet("/edit")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());

		HttpSession session = request.getSession();
		User oldUser = getOldUser(request);

		session.setAttribute("oldUser", oldUser);
		if (session.getAttribute("formUser") == null) {
			FormUser formUser = new FormUser();
			formUser.setLogin_id(oldUser.getLogin_id());
			formUser.setName(oldUser.getName());
			formUser.setStore(String.valueOf(oldUser.getStore()));
			formUser.setPosition(String.valueOf(oldUser.getPosition()));
			session.setAttribute("formUser", formUser);
		}

		StoresService storesService = new StoresService();
		PositionsService positionsService = new PositionsService();
		ArrayList<Store> storeList = storesService.getStores();
		ArrayList<Position> positionList = positionsService.getPositions();
		session.setAttribute("stores", storeList);
		session.setAttribute("positions", positionList);

	    request.getRequestDispatcher("/editView.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		HashMap<String, ArrayList<String>> errorMessages = new HashMap<>();

		if (isValid(request, errorMessages)) {
			User newUser = getNewUser(request);
			User user = (User)session.getAttribute("user");

			UserService service = new UserService();

			try {
				service.updateUser(newUser);
        		if (newUser.getId() == user.getId()) {
        			String userId = String.valueOf(newUser.getId());

        			UserService userService = new UserService();
        			user = userService.getUserWhereId(userId);
        			session.setAttribute("user", user);
        		}
        		response.sendRedirect("./");
			} catch (NoRowsUpdatedRuntimeException e) {
				HashMap<String, ArrayList<String>> errorMessagesMap = new HashMap<>();
				ArrayList<String> messages = new ArrayList<String>();
	            messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
	            errorMessagesMap.put("management", messages);
	            session.setAttribute("errorMessages", errorMessagesMap);
	            response.sendRedirect("management");
	            return;
	        }
		} else {
			FormUser formUser = getFormUser(request);
			session.setAttribute("formUser", formUser);
            session.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("/editView.jsp").forward(request, response);
		}
	}

	/**
	 * 変更前のユーザー情報を取得
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	private User getOldUser(HttpServletRequest request) throws IOException, ServletException {
		UserService service = new UserService();
		String id = request.getParameter("userId");
		User oldUser = service.getUserWhereId(id);
        return oldUser;
    }

	/**
	 * 変更後のユーザー情報取得
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	private User getNewUser(HttpServletRequest request) throws IOException, ServletException {
		User newUser = new User();

		newUser.setLogin_id(request.getParameter("newLogin_id"));
		newUser.setName(request.getParameter("newName"));
		newUser.setStore(Integer.parseInt(request.getParameter("newStore")));
		newUser.setPosition(Integer.parseInt(request.getParameter("newPosition")));
		newUser.setId(Integer.parseInt(request.getParameter("userId")));

		String newPassword = request.getParameter("newPassword");
		if (newPassword.isEmpty()) {
			newUser.setPassword("");
		} else {
			newUser.setPassword(request.getParameter("newPassword"));
		}

        return newUser;
    }

	/**
	 * 入力フォームのユーザー情報を取得
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	private FormUser getFormUser(HttpServletRequest request) throws IOException, ServletException {
		FormUser formUser = new FormUser();
		formUser.setLogin_id(request.getParameter("newLogin_id"));
		formUser.setName(request.getParameter("newName"));
		formUser.setStore(request.getParameter("newStore"));
		formUser.setPosition(request.getParameter("newPosition"));
        return formUser;
    }

	/**
	 * 入力フォームのチェック
	 * @param request
	 * @param errorMessages
	 * @return
	 */
	private boolean isValid(HttpServletRequest request, HashMap<String, ArrayList<String>> errorMessages) {
        String login_id = request.getParameter("newLogin_id");
        String oldLogin_id = request.getParameter("oldLogin_id");
        String password = request.getParameter("newPassword");
        String name = request.getParameter("newName");
        String store = request.getParameter("newStore");
        String position = request.getParameter("newPosition");
        String confirmation = request.getParameter("confirmation");
        boolean result = true;

        if (login_id.matches("^[\\p{Alnum}]{6,20}+$")) {
        	UserService service = new UserService();
        	boolean isNot = service.isNotUserExist(login_id);
        	if (!isNot && (!login_id.equals(oldLogin_id))) {
        		ArrayList<String> messages = new ArrayList<>();
        		messages.add("※登録済みのログインIDです。");
        		errorMessages.put("newLogin_id", messages);
        		result = false;
        	} else {
        		errorMessages.put("newLogin_id", null);
        	}
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※6文字以上20文字以下（半角英数字）で入力してください。");
        	errorMessages.put("newLogin_id", messages);
        	result = false;
        }

        if (password.matches("^[\\p{Alnum}|\\p{Punct}]{6,20}+$") || (password.isEmpty())) {
        	errorMessages.put("newPassword", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※6文字以上20文字以下（記号を含む全ての半角文字）で入力してください。");
        	errorMessages.put("newPassword", messages);
        	result = false;
        }

        if (name.matches("^.{1,10}+$") && !StringUtils.isBlank(name)) {
        	errorMessages.put("newName", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※1文字以上10文字以下で入力してください。");
        	errorMessages.put("newName", messages);
        	result = false;
        }

        if (!store.equals("0")) {
        	errorMessages.put("newStore", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※未選択です。");
        	errorMessages.put("newStore", messages);
        	result = false;
        }

        if (!position.equals("0")) {
        	errorMessages.put("newPosition", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※未選択です");
        	errorMessages.put("newPosition", messages);
        	result = false;
        }

        if (password.equals(confirmation)) {
        	errorMessages.put("confirmation", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※パスワードが一致しません。");
        	errorMessages.put("confirmation", messages);
        	result = false;
        }

        return result;
    }
}

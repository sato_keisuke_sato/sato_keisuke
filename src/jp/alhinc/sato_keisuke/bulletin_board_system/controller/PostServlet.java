package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Post;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.PostService;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.DateUtil;

/**
 * Servlet implementation class postController
 */
@WebServlet("/post")
public class PostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/postView.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
        Post post = getPost(request);

        HashMap<String, ArrayList<String>> errorMessages = new HashMap<>();
		if (isValid(request, errorMessages)) {
			PostService postService = new PostService();
        	postService.insertPost(post);
        	response.sendRedirect("./");

		} else {
			request.setAttribute("formPost", post);
            session.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("/postView.jsp").forward(request, response);
		}
	}

	/**
	 * 投稿内容を取得
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	private Post getPost(HttpServletRequest request) throws IOException, ServletException {
		HttpSession session = request.getSession();
		Post post = new Post();

		Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.defaultDateFormat);
        User user = (User)session.getAttribute("user");

		String subject = request.getParameter("subject");
		String text = request.getParameter("text");
		String category = request.getParameter("category");
		String regiDate = sdf.format(cal.getTime());
		int regiUser = user.getId();

        post.setSubject(subject);
        post.setText(text);
        post.setCategory(category);
        post.setRegistered_date(regiDate);
        post.setRegistered_user(regiUser);
        return post;
    }

	/**
	 * 入力フォームの違反チェック
	 * @param request
	 * @param errorMessages
	 * @return
	 */
	private boolean isValid(HttpServletRequest request, HashMap<String, ArrayList<String>> errorMessages) {
        String subject = request.getParameter("subject");
        String category = request.getParameter("category");
        String text = request.getParameter("text");
        boolean result = true;

        if (subject.matches("^.{1,30}+$") && !StringUtils.isBlank(subject)) {
        	errorMessages.put("subject", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※1文字以上30文字以下で入力してください。");
        	errorMessages.put("subject", messages);
        	result = false;
        }

        if (category.matches("^.{1,10}+$") && !StringUtils.isBlank(category)) {
        	errorMessages.put("category", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※1文字以上10文字以下で入力してください。");
        	errorMessages.put("category", messages);
        	result = false;
        }

        String regex = "^.{1,1000}+$";
        Pattern pat = Pattern.compile(regex, Pattern.DOTALL);
        Matcher mat = pat.matcher(text);

        if (mat.matches() && !StringUtils.isBlank(text)) {
        	errorMessages.put("text", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※1文字以上1000文字以下で入力してください。");
        	errorMessages.put("text", messages);
        	result = false;
        }

        return result;
    }

}

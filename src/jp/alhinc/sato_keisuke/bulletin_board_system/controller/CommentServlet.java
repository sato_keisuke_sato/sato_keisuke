package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Comment;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.HomeBean;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.HomeService;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.DateUtil;

/**
 * Servlet implementation class homeContoroller
 */
@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpServletResponse res404 = (HttpServletResponse) response;
		res404.sendError(HttpServletResponse.SC_NOT_FOUND);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		HomeService homeService = new HomeService();

		HashMap<String, ArrayList<String>> errorMessages = new HashMap<>();
		if (isValid(request, errorMessages)) {
			Comment comment = getComment(request);
			homeService.insertComment(comment);
			HomeBean home = homeService.getFormValue(request);
			session.setAttribute("home", home);
			response.sendRedirect("./");

		} else {
			session.setAttribute("errorMessages", errorMessages);
			session.setAttribute("formComment", request.getParameter("comment"));
			session.setAttribute("formPost_id", request.getParameter("post_id"));
			HomeBean home = homeService.getFormValue(request);
			session.setAttribute("home", home);
			response.sendRedirect("./");
		}
	}

	/**
	 * 入力情報の取得
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	private Comment getComment(HttpServletRequest request) throws IOException, ServletException {
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());
		HttpSession session = request.getSession();
		Comment comment = new Comment();
		Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.defaultDateFormat);

		comment.setText(request.getParameter("comment"));
		comment.setRegistered_date(sdf.format(cal.getTime()));
		comment.setRegistered_user(((User)session.getAttribute("user")).getId());
		comment.setPost_id(Integer.parseInt(request.getParameter("post_id")));
        return comment;
    }

	/**
	 * 入力違反のチェック
	 * @param request
	 * @param errorMessages
	 * @return
	 */
	private boolean isValid(HttpServletRequest request, HashMap<String, ArrayList<String>> errorMessages) {
        String comment = request.getParameter("comment");
        String post_id = request.getParameter("post_id");
        boolean result = true;

        String regex = "^.{1,500}+$";
        Pattern pat = Pattern.compile(regex, Pattern.DOTALL);
        Matcher mat = pat.matcher(comment);

        if (mat.matches() && !StringUtils.isBlank(comment)) {
        	errorMessages.put("comment", null);
        } else {
    		ArrayList<String> messages = new ArrayList<>();
        	messages.add("※1文字以上500文字以下で入力してください。");
        	errorMessages.put(post_id, messages);
        	result = false;
        }
        return result;
    }
}

package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.LoginService;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.PermissionUtil;

/**
 * Servlet implementation class loginController
 */
@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		if (PermissionUtil.getLoginState(session)) {
			response.sendRedirect("./");
		} else {
		    request.getRequestDispatcher("/loginView.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");

		LoginService loginService = new LoginService();
		User user = loginService.login(login_id, password);

		HttpSession session = request.getSession(true);
		if (user != null) {
			session.setAttribute("user", user);
			response.sendRedirect("./");

		} else {
			request.setAttribute("formLogin_id", login_id);
			HashMap<String, ArrayList<String>> loginErrMap = new HashMap<>();
			ArrayList<String> messages = new ArrayList<String>();
            messages.add("※ログインに失敗しました。");
            loginErrMap.put("login", messages);
            session.setAttribute("errorMessages", loginErrMap);

            request.getRequestDispatcher("/loginView.jsp").forward(request, response);
		}
	}
}

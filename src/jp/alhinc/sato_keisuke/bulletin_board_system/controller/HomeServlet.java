package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Comment;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.HomeBean;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Post;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.HomeService;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.DateUtil;

/**
 * Servlet implementation class homeContoroller
 */
@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HomeService service = new HomeService();
		HomeBean home = service.getFormValue(request);

		String category = home.getCategory();
		String from = DateUtil.getFromDate(home);
		String to = DateUtil.getToDate(home);
		System.out.println(category);
		System.out.println(from);
		System.out.println(to);

		DateUtil DateUtil = new DateUtil();
		request.setAttribute("DateUtil", DateUtil);
		HomeService homeService = new HomeService();
		ArrayList<Post> postList = homeService.getPosts(category, from, to, home.getCurrentPage());
		ArrayList<Comment> commentList = homeService.getComments(postList);
		home.setAllPages(homeService.getAllPages(category, from, to));

		request.setAttribute("posts", postList);
		request.setAttribute("comments", commentList);
		request.setAttribute("home", home);

	    request.getRequestDispatcher("/homeView.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//404
		HttpServletResponse res404 = (HttpServletResponse) response;
		res404.sendError(HttpServletResponse.SC_NOT_FOUND);
	}

}

package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.FormUser;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Position;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Store;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.PositionsService;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.StoresService;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.UserService;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.PermissionUtil;

/**
 * Servlet implementation class homeContoroller
 */
@WebServlet("/signUp")
public class signUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public signUpServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		StoresService storesService = new StoresService();
		PositionsService positionsService = new PositionsService();
		ArrayList<Store> storeList = storesService.getStores();
		ArrayList<Position> positionList = positionsService.getPositions();
		session.setAttribute("stores", storeList);
		session.setAttribute("positions", positionList);

		request.getRequestDispatcher("/signUpView.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		HashMap<String, ArrayList<String>> errorMessages = new HashMap<>();
		if (isValid(request, errorMessages)) {
			User user = new User();
			user.setLogin_id(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setStore(Integer.parseInt(request.getParameter("store")));
			user.setPosition(Integer.parseInt(request.getParameter("position")));
			user.setValidity(PermissionUtil.validUser);

			UserService userService = new UserService();
        	userService.signUp(user);
			response.sendRedirect("./");

		} else {
			//入力違反
			FormUser formUser = getFormUser(request);
			request.setAttribute("formUser", formUser);
            session.setAttribute("errorMessages", errorMessages);
    	    request.getRequestDispatcher("/signUpView.jsp").forward(request, response);
		}
	}

	/**
	 * 入力内容の取得
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	private FormUser getFormUser(HttpServletRequest request) throws IOException, ServletException {
		FormUser formUser = new FormUser();
		formUser.setLogin_id(request.getParameter("login_id"));
		formUser.setName(request.getParameter("name"));
		formUser.setStore(request.getParameter("store"));
		formUser.setPosition(request.getParameter("position"));
        return formUser;
    }

	/**
	 * 入力フォームの違反チェック
	 * @param request
	 * @param errorMessages
	 * @return
	 */
	private boolean isValid(HttpServletRequest request, HashMap<String, ArrayList<String>> errorMessages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String store = request.getParameter("store");
        String position = request.getParameter("position");
        String confirmation = request.getParameter("confirmation");
        boolean result = true;

        if (login_id.matches("^[\\p{Alnum}]{6,20}+$")) {
        	UserService service = new UserService();
        	boolean isNot = service.isNotUserExist(login_id);
        	if (!isNot) {
        		ArrayList<String> messages = new ArrayList<>();
        		messages.add("※登録済みのログインIDです。");
        		errorMessages.put("login_id", messages);
        		result = false;
        	} else {
        		errorMessages.put("login_id", null);
        	}
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※6文字以上20文字以下（半角英数字）で入力してください。");
        	errorMessages.put("login_id", messages);
        	result = false;
        }

        if (password.matches("^[\\p{Alnum}|\\p{Punct}]{6,20}+$")) {
        	errorMessages.put("password", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※6文字以上20文字以下（記号を含む全ての半角文字）で入力してください。");
        	errorMessages.put("password", messages);
        	result = false;
        }

        if (name.matches("^.{1,10}+$") && !StringUtils.isBlank(name)) {
        	errorMessages.put("name", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※1文字以上10文字以下で入力してください。");
        	errorMessages.put("name", messages);
        	result = false;
        }

        if (!store.equals("0")) {
        	errorMessages.put("store", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※未選択です。");
        	errorMessages.put("store", messages);
        	result = false;
        }

        if (!position.equals("0")) {
        	errorMessages.put("position", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※未選択です。");
        	errorMessages.put("position", messages);
        	result = false;
        }

        if (password.equals(confirmation)) {
        	errorMessages.put("confirmation", null);
        } else {
        	ArrayList<String> messages = new ArrayList<>();
        	messages.add("※パスワードが一致しません。");
        	errorMessages.put("confirmation", messages);
        	result = false;
        }

        return result;
    }
}

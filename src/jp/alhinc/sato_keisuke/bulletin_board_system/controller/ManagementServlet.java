package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;
import jp.alhinc.sato_keisuke.bulletin_board_system.exception.NoRowsUpdatedRuntimeException;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.UserService;

/**
 * Servlet implementation class postController
 */
@WebServlet("/management")
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManagementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		viewForword(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("changeUserId");
		int userValidity = Integer.parseInt(request.getParameter("changeUserValidity"));

        try {
            new UserService().changeUserValidity(id, userValidity);
        } catch (NoRowsUpdatedRuntimeException e) {
        	HttpSession session = request.getSession();
			HashMap<String, ArrayList<String>> errorMessagesMap = new HashMap<>();
			ArrayList<String> messages = new ArrayList<String>();
            messages.add("※更新できませんでした。");
            errorMessagesMap.put("error", messages);
            session.setAttribute("errorMessages", errorMessagesMap);
        }
        viewForword(request, response);
	}

	private void viewForword (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserService userService = new UserService();
		ArrayList<User> userList = userService.getUsers();
		request.setAttribute("userList", userList);
    	request.getRequestDispatcher("/managementView.jsp").forward(request, response);
	}
}

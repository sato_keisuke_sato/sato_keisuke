package jp.alhinc.sato_keisuke.bulletin_board_system.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Comment;
import jp.alhinc.sato_keisuke.bulletin_board_system.exception.SQLRuntimeException;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;

public class CommentsDao {
	private ArrayList<Comment> toCommentList(ResultSet rs) throws SQLException {
        ArrayList<Comment> commentList = new ArrayList<>();
        try {
            while (rs.next()) {
            	String text = rs.getString("text");
				String registered_date = rs.getString("registered_date");
				int registered_user = rs.getInt("registered_user");
				int post_id = rs.getInt("post_id");
				int id = rs.getInt("id");

                Comment comment = new Comment();
                comment.setText(text);
				comment.setRegistered_date(registered_date);
				comment.setRegistered_user(registered_user);
				comment.setPost_id(post_id);
				comment.setId(id);
                commentList.add(comment);
            }
            return commentList;
        } finally {
            CloseableUtil.close(rs);
        }
    }

	private ArrayList<Comment> toInnerCommentList(ResultSet rs) throws SQLException {
		ArrayList<Comment> commentList = new ArrayList<>();
        try {
            while (rs.next()) {
            	String text = rs.getString("comments.text");
				String registered_date = rs.getString("comments.registered_date");
				int registered_user = rs.getInt("comments.registered_user");
				int post_id = rs.getInt("comments.post_id");
				int id = rs.getInt("comments.id");
				String registered_userName = rs.getString("users.name");

                Comment comment = new Comment();
                comment.setText(text);
				comment.setRegistered_date(registered_date);
				comment.setRegistered_user(registered_user);
				comment.setRegistered_userName(registered_userName);
				comment.setPost_id(post_id);
				comment.setId(id);
                commentList.add(comment);
            }
            return commentList;
        } finally {
            CloseableUtil.close(rs);
        }
	}

	/**
	 * コメントを返す（全量）
	 * @param connection
	 * @return コメントリスト
	 */
	public ArrayList<Comment> getComments(Connection connection) {
		Statement st = null;
	    try {
	        String sql = "";
			sql = "select * from comments INNER JOIN users ON comments.registered_user = users.id order by registered_date asc";
			st = connection.createStatement();
	        ResultSet rs = st.executeQuery(sql);
	        ArrayList<Comment> commentList = toInnerCommentList(rs);
	        if (commentList.isEmpty() == true) {
	            return null;
	        } else {
	            return commentList;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(st);
	    }
	}

	/**
	 *
	 * @return 0:err
	 */
	public int insert (Connection connection, Comment comment) {
		PreparedStatement ps = null;
	    try {
	        String sql = "";
			sql = "INSERT INTO comments (text, registered_date, registered_user, post_id) VALUES (?, ?, ?, ?)";
			ps = connection.prepareStatement(sql);
	        ps.setString(1, comment.getText());
	        ps.setString(2, comment.getRegistered_date());
	        ps.setInt(3, comment.getRegistered_user());
	        ps.setInt(4, comment.getPost_id());
	        int rs = ps.executeUpdate();

	        return rs;
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(ps);
	    }
	}

	public int delete(Connection connection, int id) {
		PreparedStatement ps = null;
	    try {
	        String sql = "";
			sql = "delete from comments where id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

	        int result = ps.executeUpdate();
	        return result;
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(ps);
	    }
	}

	public int deleteWherePost_id(Connection connection, int post_id) {
		PreparedStatement ps = null;
	    try {
	        String sql = "";
			sql = "delete from comments where post_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, post_id);

	        int result = ps.executeUpdate();
	        return result;
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(ps);
	    }
	}
}

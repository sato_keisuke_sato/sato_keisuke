package jp.alhinc.sato_keisuke.bulletin_board_system.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Store;
import jp.alhinc.sato_keisuke.bulletin_board_system.exception.SQLRuntimeException;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;

public class StoresDao {
	private ArrayList<Store> toStoreList (ResultSet rs) throws SQLException {
        ArrayList<Store> storeList = new ArrayList<>();
        try {
            while (rs.next()) {
            	String name = rs.getString("name");
            	int code = rs.getInt("code");

                Store store = new Store();
                store.setName(name);
				store.setCode(code);
                storeList.add(store);
            }
            return storeList;
        } finally {
            CloseableUtil.close(rs);
        }
    }

	public ArrayList<Store> getStores(Connection connection) {
		Statement st = null;
	    try {
	        String sql = "select * from stores";
	        st = connection.createStatement();
	        ResultSet rs = st.executeQuery(sql);
	        ArrayList<Store> storeList = toStoreList(rs);
	        if (storeList.isEmpty() == true) {
	            return null;
	        } else {
	            return storeList;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(st);
	    }
	}
}

package jp.alhinc.sato_keisuke.bulletin_board_system.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;
import jp.alhinc.sato_keisuke.bulletin_board_system.exception.NoRowsUpdatedRuntimeException;
import jp.alhinc.sato_keisuke.bulletin_board_system.exception.SQLRuntimeException;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.PermissionUtil;

public class UsersDao {
	/**
	 *
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private ArrayList<User> toInnerUserList(ResultSet rs) throws SQLException {

       ArrayList<User> users = new ArrayList<>();
       try {
           while (rs.next()) {
               String login_id = rs.getString("users.login_id");
               String password = rs.getString("users.password");
               String name = rs.getString("users.name");
               int store = rs.getInt("users.store");
               String storeName = rs.getString("stores.name");
               int position = rs.getInt("users.position");
               String positionName = rs.getString("positions.name");
               int validity = rs.getInt("users.validity");
               int id = rs.getInt("users.id");

               User bean = new User();
               bean.setLogin_id(login_id);
               bean.setPassword(password);
               bean.setName(name);
               bean.setStore(store);
               bean.setStoreName(storeName);
               bean.setPosition(position);
               bean.setPositionName(positionName);
               bean.setValidity(validity);
               bean.setId(id);
               users.add(bean);
           }
           return users;
       } finally {
           CloseableUtil.close(rs);
       }
	}

	private ArrayList<User> toUserList(ResultSet rs) throws SQLException {

	       ArrayList<User> users = new ArrayList<>();
	       try {
	           while (rs.next()) {
	               String login_id = rs.getString("login_id");
	               String password = rs.getString("password");
	               String name = rs.getString("name");
	               int store = rs.getInt("store");
	               int position = rs.getInt("position");
	               int validity = rs.getInt("validity");

	               User bean = new User();
	               bean.setLogin_id(login_id);
	               bean.setPassword(password);
	               bean.setName(name);
	               bean.setStore(store);
	               bean.setPosition(position);
	               bean.setValidity(validity);
	               users.add(bean);
	           }
	           return users;
	       } finally {
	           CloseableUtil.close(rs);
	       }
	   }

	public int insert(Connection connection, User user) {
		int result = 0;
	    PreparedStatement ps = null;
	    try {
	        StringBuilder sql = new StringBuilder();
	        sql.append("INSERT INTO users ( ");
	        sql.append("login_id");
	        sql.append(", password");
	        sql.append(", name");
	        sql.append(", store");
	        sql.append(", position");
	        sql.append(", validity");
	        sql.append(") VALUES (");
	        sql.append("?");   // login_id
	        sql.append(", ?"); // password
	        sql.append(", ?"); // name
	        sql.append(", ?"); // store
	        sql.append(", ?"); // position
	        sql.append(", ?"); // user
	        sql.append(")");

	        ps = connection.prepareStatement(sql.toString());
	        ps.setString(1, user.getLogin_id());
	        ps.setString(2, user.getPassword());
	        ps.setString(3, user.getName());
	        ps.setInt(4, user.getStore());
	        ps.setInt(5, user.getPosition());
	        ps.setInt(6, PermissionUtil.validUser);
	        result = ps.executeUpdate();
	        return result;
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	    	CloseableUtil.close(ps);
	    }
	}

	public User getUserWhereLoginId(Connection connection, String login_id) {
		PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE login_id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, login_id);
	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(ps);
	    }
	}

	public User getUserWhereId(Connection connection, String id) {
		PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users INNER JOIN stores ON users.store = stores.code INNER JOIN positions ON users.position = positions.code WHERE id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, id);
	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toInnerUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(ps);
	    }
	}

	/**
	 * 指定の有効ユーザを取得する ログイン用
	 * @param login_id
	 * @param password
	 * @return ユーザー
	 */
	public User getUser(Connection connection, String login_id, String password) {
		PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users INNER JOIN stores ON users.store = stores.code INNER JOIN positions ON users.position = positions.code WHERE login_id = ? AND password = ? AND validity = ?";
	        ps = connection.prepareStatement(sql);
	        ps.setString(1, login_id);
	        ps.setString(2, password);
	        ps.setInt(3, PermissionUtil.validUser);
	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toInnerUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(ps);
	    }
	}

	/**
	 * 全ユーザー取得
	 * @param connection
	 * @return
	 */
	public ArrayList<User> getUsers(Connection connection) {
		Statement st = null;
	    try {
	        String sql = "SELECT * FROM users INNER JOIN stores ON users.store = stores.code INNER JOIN positions ON users.position = positions.code order by id asc";

	        st= connection.createStatement();
	        ResultSet rs = st.executeQuery(sql);
	        ArrayList<User> userList = toInnerUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else {
	            return userList;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(st);
	    }
	}

	/**
	 * ユーザー更新
	 * @param connection
	 * @param newUser
	 * @return
	 */
    public int update(Connection connection, User newUser) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  login_id = ?");
			sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", store = ?");
			sql.append(", position = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, newUser.getLogin_id());
	        ps.setString(2, newUser.getPassword());
	        ps.setString(3, newUser.getName());
	        ps.setInt(4, newUser.getStore());
	        ps.setInt(5, newUser.getPosition());
	        ps.setInt(6, newUser.getId());

			int result = ps.executeUpdate();
			if (result == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
			return result;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			CloseableUtil.close(ps);
		}
	}

    /**
     * ユーザー更新　パスワードはそのまま
     * @param connection
     * @param newUser
     * @return
     */
    public int updateWithoutPass(Connection connection, User newUser) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  login_id = ?");
			sql.append(", name = ?");
			sql.append(", store = ?");
			sql.append(", position = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, newUser.getLogin_id());
	        ps.setString(2, newUser.getName());
	        ps.setInt(3, newUser.getStore());
	        ps.setInt(4, newUser.getPosition());
	        ps.setInt(5, newUser.getId());

			int result = ps.executeUpdate();
			if (result == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
			return result;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			CloseableUtil.close(ps);
		}
	}

    /**
     * ユーザーを有効にする
     * @param connection
     * @param id
     * @return
     */
    public int userEnable(Connection connection, String id) {

		PreparedStatement ps = null;
		try {
			String sql = "UPDATE users SET validity = ? WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, PermissionUtil.validUser);
			ps.setString(2, id);

			int result = ps.executeUpdate();
			if (result == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
			return result;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			CloseableUtil.close(ps);
		}

	}

    /**
     * ユーザーを無効にする
     * @param connection
     * @param id
     * @return
     */
    public int userDisable(Connection connection, String id) {

		PreparedStatement ps = null;
		try {
			String sql = "UPDATE users SET validity = ? WHERE id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, PermissionUtil.invalidUser);
			ps.setString(2, id);

			int result = ps.executeUpdate();
			if (result == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
			return result;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			CloseableUtil.close(ps);
		}
	}
}

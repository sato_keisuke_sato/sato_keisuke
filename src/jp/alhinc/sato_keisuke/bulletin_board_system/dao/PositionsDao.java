package jp.alhinc.sato_keisuke.bulletin_board_system.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Position;
import jp.alhinc.sato_keisuke.bulletin_board_system.exception.SQLRuntimeException;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;

public class PositionsDao {
	private ArrayList<Position> toPositionList (ResultSet rs) throws SQLException {
        ArrayList<Position> positionList = new ArrayList<>();
        try {
            while (rs.next()) {
            	String name = rs.getString("name");
            	int code = rs.getInt("code");

                Position position = new Position();
                position.setName(name);
				position.setCode(code);
                positionList.add(position);
            }
            return positionList;
        } finally {
            CloseableUtil.close(rs);
        }
    }

	public ArrayList<Position> getPositions(Connection connection) {
		Statement st = null;
	    try {
	        String sql = "select * from positions";
	        st = connection.createStatement();
	        ResultSet rs = st.executeQuery(sql);
	        ArrayList<Position> positionList = toPositionList(rs);
	        if (positionList.isEmpty() == true) {
	            return null;
	        } else {
	            return positionList;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(st);
	    }
	}
}
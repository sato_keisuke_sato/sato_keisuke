package jp.alhinc.sato_keisuke.bulletin_board_system.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Post;
import jp.alhinc.sato_keisuke.bulletin_board_system.exception.SQLRuntimeException;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;

public class PostsDao {

	private ArrayList<Post> toPostList(ResultSet rs) throws SQLException {
        ArrayList<Post> posts = new ArrayList<>();
        try {
            while (rs.next()) {
            	String subject = rs.getString("subject");
            	String text = rs.getString("text");
				String category = rs.getString("category");
				String registered_date = rs.getString("registered_date");
				int registered_user = rs.getInt("registered_user");
				int id = rs.getInt("id");

                Post post = new Post();
                post.setSubject(subject);
				post.setText(text);
				post.setCategory(category);
				post.setRegistered_date(registered_date);
				post.setRegistered_user(registered_user);
				post.setId(id);
                posts.add(post);
            }
            return posts;
        } finally {
            CloseableUtil.close(rs);
        }
    }

	private ArrayList<Post> toInnerPostList(ResultSet rs) throws SQLException {
		ArrayList<Post> posts = new ArrayList<>();
        try {
            while (rs.next()) {
            	String subject = rs.getString("posts.subject");
            	String text = rs.getString("posts.text");
				String category = rs.getString("posts.category");
				String registered_date = rs.getString("posts.registered_date");
				int registered_user = rs.getInt("posts.registered_user");
				int id = rs.getInt("posts.id");
				String registered_userName = rs.getString("users.name");

                Post post = new Post();
                post.setSubject(subject);
				post.setText(text);
				post.setCategory(category);
				post.setRegistered_date(registered_date);
				post.setRegistered_user(registered_user);
				post.setRegistered_userName(registered_userName);
				post.setId(id);
                posts.add(post);
            }
            return posts;
        } finally {
            CloseableUtil.close(rs);
        }
	}

	/**
	 * 投稿を返す
	 * @param connection
	 * @param category
	 * @param from
	 * @param to
	 * @return 投稿リスト
	 */
	public ArrayList<Post> getPosts(Connection connection, String category, String from, String to, int start, int limit) {
		PreparedStatement ps = null;
	    try {
	        String sql = "";
	        if (category.equals("")) {
				if (from.equals("")) {
					sql = "select * from posts INNER JOIN users ON posts.registered_user = users.id order by registered_date desc limit ?,5";
					ps = connection.prepareStatement(sql);
					ps.setInt(1, start);
				} else {
					sql = "select * from posts INNER JOIN users ON posts.registered_user = users.id where registered_date between ? and ? order by registered_date desc limit ?,5";
					ps = connection.prepareStatement(sql);
			        ps.setString(1, from);
			        ps.setString(2, to);
			        ps.setInt(3, start);
				}
			} else {
				sql = "select * from posts INNER JOIN users ON posts.registered_user = users.id where (category like ?) AND (registered_date between ? and ?) order by registered_date desc limit ?,5";
				ps = connection.prepareStatement(sql);
		        ps.setString(1, "%" + category + "%");
		        ps.setString(2, from);
		        ps.setString(3, to);
		        ps.setInt(4, start);
			}

	        ResultSet rs = ps.executeQuery();
	        ArrayList<Post> postList = toInnerPostList(rs);

	        if (postList.isEmpty() == true) {
	            return null;
	        } else {
	            return postList;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(ps);
	    }
	}

	public int count(Connection connection, String category, String from, String to) {
		PreparedStatement ps = null;
	    try {
	        String sql = "";
	        if (category.equals("")) {
				if (from.equals("")) {
					sql = "select count(*) AS cnt from posts";
					ps = connection.prepareStatement(sql);
				} else {
					sql = "select count(*) AS cnt from posts where registered_date between ? and ?";
					ps = connection.prepareStatement(sql);
			        ps.setString(1, from);
			        ps.setString(2, to);
				}
			} else {
				sql = "select count(*) AS cnt from posts where (category like ?) AND (registered_date between ? and ?)";
				ps = connection.prepareStatement(sql);
				ps.setString(1, "%" + category + "%");
		        ps.setString(2, from);
		        ps.setString(3, to);
			}

	        ResultSet rs = ps.executeQuery();
	        int cnt = -1;
	        while (rs.next()) {
	        	cnt = rs.getInt("cnt");
	        }
	        return cnt;
	    } catch (SQLException e) {
	    	e.printStackTrace();
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(ps);
	    }
	}

	public int delete(Connection connection, int id) {
		PreparedStatement ps = null;
	    try {
	        String sql = "";
			sql = "delete from posts where id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

	        int result = ps.executeUpdate();
	        return result;
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        CloseableUtil.close(ps);
	    }
	}

	public int insert(Connection connection, Post post) {
	    PreparedStatement ps = null;
	    try {
	        StringBuilder sql = new StringBuilder();
	        sql.append("INSERT INTO posts ( ");
	        sql.append("subject");
	        sql.append(", text");
	        sql.append(", category");
	        sql.append(", registered_date");
	        sql.append(", registered_user");
	        sql.append(") VALUES (");
	        sql.append("?");   // subject
	        sql.append(", ?"); // text
	        sql.append(", ?"); // category
	        sql.append(", ?"); // regi_date
	        sql.append(", ?"); // regi_user
	        sql.append(")");

	        ps = connection.prepareStatement(sql.toString());

	        ps.setString(1, post.getSubject());
	        ps.setString(2, post.getText());
	        ps.setString(3, post.getCategory());
	        ps.setString(4, post.getRegistered_date());
	        ps.setInt(5, post.getRegistered_user());
	        int result = ps.executeUpdate();
	        return result;
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	    	CloseableUtil.close(ps);
	    }
	}
}

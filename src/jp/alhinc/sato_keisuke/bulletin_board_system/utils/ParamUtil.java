package jp.alhinc.sato_keisuke.bulletin_board_system.utils;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * パラメータユーティリティー
 */
public class ParamUtil {

    public static String nullCheck(HttpServletRequest request, String key, String value) throws IOException, ServletException {
    	if (request.getParameter(key) != null) {
    		value = request.getParameter(key);
    	}
    	return value;
    }

    public static int nullCheck(HttpServletRequest request, String key, int value) throws IOException, ServletException {
    	if (request.getParameter(key) != null) {
    		value = Integer.parseInt(request.getParameter(key));
    	}
    	return value;
    }
}
package jp.alhinc.sato_keisuke.bulletin_board_system.utils;

import javax.servlet.http.HttpSession;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;

public class PermissionUtil {
	public static final int editPerStore = 1;
	public static final int editPerPosition = 1;
	public static final int invalidUser = 0;
	public static final int validUser = 1;

	/**
	 * セッションのユーザー情報を確認し、ログインの有無を確認する
	 * @param session
	 * @return true:ログイン中 false:ログアウト中
	 */
	public static boolean getLoginState (HttpSession session) {
		boolean result =false;
		User user = (User)session.getAttribute("user");
		if (user != null) {
			result = true;
		}
		return result;
	}

	/**
	 * 本社総務部向けページの権限チェック
	 * @return
	 */
	public static boolean checkPermission (HttpSession session) {
		boolean result = false;
		User user = (User)session.getAttribute("user");
		if (user != null) {
			if ((user.getStore() == editPerStore) && (user.getPosition() == editPerPosition)) {
				result = true;
			}
		}
		return result;
	}
}

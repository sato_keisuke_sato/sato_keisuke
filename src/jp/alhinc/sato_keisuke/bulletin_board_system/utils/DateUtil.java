package jp.alhinc.sato_keisuke.bulletin_board_system.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.HomeBean;

public class DateUtil {
	public static final String defaultFromDate = "1950-1-1 00:00:00";
	public static final String defaultDateFormat = "yyyy-MM-dd HH:mm:ss";
	public static final String defaultYearFormat = "yyyy";
	public static final String defaultMonthFormat = "M";
	public static final String defaultDayFormat = "d";

	public static String getFromDate (HomeBean home) {
		String year = home.getFromYear();
		String month = home.getFromMonth();
		String day = home.getFromDay();
		String date;
		date = year + "-" + month + "-" + day + " 00:00:00";

		return date;
	}

	public static String getToDate (HomeBean home) {
		String year = home.getToYear();
		String month = home.getToMonth();
		String day = home.getToDay();
		String date;
		date = year + "-" + month + "-" + day + " 23:59:59";
		return date;
	}

	public static String getDayDiff (String from) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date dateFrom = null;
	    Date dateTo = null;
	    Calendar cal = Calendar.getInstance();
		try {
	        dateFrom = sdf.parse(from);
	        dateTo = sdf.parse(sdf.format(cal.getTime()));
	    } catch (ParseException e) {
	        e.printStackTrace();
	    }

	    // 日付をlong値に変換します。
	    long dateTimeFrom = dateFrom.getTime();
	    long dateTimeTo = dateTo.getTime();

	    // 差分の時間を算出します。
	    long dayDiff = ( dateTimeTo - dateTimeFrom  ) / 1000;
	    String dif = "";
	    if (dayDiff < 60) {
	    	//秒
	    	dif = dayDiff + "秒前";
	    } else if (dayDiff < 3600) {
	    	//分
	    	dif = (dayDiff / 60) + "分前";
	    } else if (dayDiff < 86400) {
	    	//時間
	    	dif = (dayDiff / 3600) + "時間前";
	    } else {
	    	//日
	    	dif = (dayDiff / 86400) + "日前";
	    }
	    return dif;
	}
}

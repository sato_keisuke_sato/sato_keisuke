package jp.alhinc.sato_keisuke.bulletin_board_system.service;

import java.sql.Connection;
import java.util.ArrayList;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Position;
import jp.alhinc.sato_keisuke.bulletin_board_system.dao.PositionsDao;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.DBUtil;

public class PositionsService {
	public ArrayList<Position> getPositions () {
		ArrayList<Position> positions = new ArrayList<>();
		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			PositionsDao positionsDao = new PositionsDao();
			positions = positionsDao.getPositions(connection);

			DBUtil.commit(connection);

			return positions;
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}
}

//package jp.alhinc.sato_keisuke.bulletin_board_system.service;
//
//import java.sql.Connection;
//import java.util.ArrayList;
//
//import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;
//import jp.alhinc.sato_keisuke.bulletin_board_system.dao.UsersDao;
//import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;
//import jp.alhinc.sato_keisuke.bulletin_board_system.utils.DBUtil;
//import jp.alhinc.sato_keisuke.bulletin_board_system.utils.PermissionUtil;
//
//public class ManagementService {
//	public ArrayList<User> getUsers () {
//		ArrayList<User> postList = new ArrayList<>();
//		Connection connection = null;
//
//		try {
//			connection = DBUtil.getConnection();
//
//			UsersDao usersDao = new UsersDao();
//			postList = usersDao.getUsers(connection);
//
//			DBUtil.commit(connection);
//
//			return postList;
//		} catch (RuntimeException e) {
//			DBUtil.rollback(connection);
//			throw e;
//		} catch (Error e) {
//			DBUtil.rollback(connection);
//			throw e;
//		} finally {
//			CloseableUtil.close(connection);
//		}
//	}
//
//	/**
//	 * ユーザーの有効性を切り替える
//	 * @param user
//	 * @return 1:成功
//	 */
//	public int changeUserValidity (String id, int validity) {
//		Connection connection = null;
//		try {
//			connection = DBUtil.getConnection();
//
//			UsersDao usersDao = new UsersDao();
//			int result = -1;
//
//			if (validity == PermissionUtil.validUser) {
//				result = usersDao.userDisable(connection, id);
//			} else {
//				result = usersDao.userEnable(connection, id);
//			}
//			DBUtil.commit(connection);
//
//			return result;
//		} catch (RuntimeException e) {
//			DBUtil.rollback(connection);
//			throw e;
//		} catch (Error e) {
//			DBUtil.rollback(connection);
//			throw e;
//		} finally {
//			CloseableUtil.close(connection);
//		}
//	}
//}

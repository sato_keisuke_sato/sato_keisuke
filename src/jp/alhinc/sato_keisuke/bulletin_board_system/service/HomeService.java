package jp.alhinc.sato_keisuke.bulletin_board_system.service;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Comment;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.HomeBean;
import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Post;
import jp.alhinc.sato_keisuke.bulletin_board_system.dao.CommentsDao;
import jp.alhinc.sato_keisuke.bulletin_board_system.dao.PostsDao;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.DBUtil;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.ParamUtil;

public class HomeService {
	public ArrayList<Post> getPosts (String category, String from, String to, int pageNum) {
		ArrayList<Post> postList = new ArrayList<>();
		Connection connection = null;

		int start = (pageNum - 1) * HomeBean.dispLimitNum;
		try {
			connection = DBUtil.getConnection();

			PostsDao postsDao = new PostsDao();
			postList = postsDao.getPosts(connection, category, from, to, start, HomeBean.dispLimitNum);
			replaceAllPostsNl(postList);

			DBUtil.commit(connection);

			return postList;
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}

	public ArrayList<Comment> getComments (ArrayList<Post> postList) {
		ArrayList<Comment> commentList = null;
		ArrayList<Comment> commentApList = null;
		if (postList != null) {
			commentApList = new ArrayList<>();
			Connection connection = null;
			try {
				connection = DBUtil.getConnection();

				CommentsDao commentsDao = new CommentsDao();
				commentList = commentsDao.getComments(connection);

				ArrayList<Integer> post_idList = new ArrayList<>();
				for (int i = 0; i < postList.size(); i++) {
					post_idList.add(postList.get(i).getId());
				}
				if (commentList != null) {
					for (int i = 0; i < commentList.size(); i++) {
						if (post_idList.contains(commentList.get(i).getPost_id())) {
							commentApList.add(commentList.get(i));
						}
					}
				}

				DBUtil.commit(connection);

				replaceAllCommentsNl(commentApList);

				return commentApList;
			} catch (RuntimeException e) {
				DBUtil.rollback(connection);
				throw e;
			} catch (Error e) {
				DBUtil.rollback(connection);
				throw e;
			} finally {
				CloseableUtil.close(connection);
			}
		} else {
			return null;
		}
	}

	private void replaceAllPostsNl (ArrayList<Post> postList) {
		if (postList != null) {
			for (int i = 0; i < postList.size(); i++) {
				Post post = postList.get(i);
				String text = post.getText();

				String regex = System.getProperty("line.separator");
		        Pattern pat = Pattern.compile(regex, Pattern.DOTALL);
		        Matcher mat = pat.matcher(text);
		        text = mat.replaceAll("<br>");
				post.setText(text);
				postList.set(i, post);
			}
		}
	}

	private void replaceAllCommentsNl (ArrayList<Comment> commentList) {
		if (commentList != null) {
			for (int i = 0; i < commentList.size(); i++) {
				Comment comment = commentList.get(i);
				String text = comment.getText();

				String regex = System.getProperty("line.separator");
		        Pattern pat = Pattern.compile(regex, Pattern.DOTALL);
		        Matcher mat = pat.matcher(text);
		        text = mat.replaceAll("<br>");
				comment.setText(text);
				commentList.set(i, comment);
			}
		}
	}

	public int getAllPages (String category, String from, String to) {
		Connection connection = null;
		try {
			connection = DBUtil.getConnection();

			PostsDao postsDao = new PostsDao();
			int count = postsDao.count(connection, category, from, to);

			DBUtil.commit(connection);
			int allPages = count / HomeBean.dispLimitNum;
			if ((count % HomeBean.dispLimitNum) > 0) {
				allPages++;
			}
			return allPages;
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}

	public void insertComment (Comment comment) {
		Connection connection = null;

		try {
			connection = DBUtil.getConnection();
			CommentsDao commentsDao = new CommentsDao();
			int result;
			result = commentsDao.insert(connection, comment);
			DBUtil.commit(connection);

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}

	public int deletePost (String id) {
		Connection connection = null;

		try {
			connection = DBUtil.getConnection();
			PostsDao postsDao = new PostsDao();
			int result;
			result = postsDao.delete(connection, Integer.parseInt(id));
			DBUtil.commit(connection);

			return result;
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}

	public int deleteComment (String id) {
		Connection connection = null;

		try {
			connection = DBUtil.getConnection();
			CommentsDao commentsDao = new CommentsDao();
			int result;
			result = commentsDao.delete(connection, Integer.parseInt(id));
			DBUtil.commit(connection);

			return result;
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}

//	public int deleteCommentWherePost_id (String post_id) {
//		Connection connection = null;
//
//		try {
//			connection = DBUtil.getConnection();
//			CommentsDao commentsDao = new CommentsDao();
//			int result;
//			result = commentsDao.deleteWherePost_id(connection, Integer.parseInt(post_id));
//			DBUtil.commit(connection);
//
//			return result;
//		} catch (RuntimeException e) {
//			DBUtil.rollback(connection);
//			throw e;
//		} catch (Error e) {
//			DBUtil.rollback(connection);
//			throw e;
//		} finally {
//			CloseableUtil.close(connection);
//		}
//	}

	public HomeBean getFormValue (HttpServletRequest request) throws IOException, ServletException {
		HttpSession session = request.getSession();
		HomeBean home;
		if ((HomeBean)session.getAttribute("home") != null) {
			home = (HomeBean)session.getAttribute("home");
			session.removeAttribute("home");
		} else {
			home = new HomeBean();
			String category = ParamUtil.nullCheck(request, "category", home.getCategory());
			String fromYear = ParamUtil.nullCheck(request, "fromYear", home.getFromYear());
			String fromMonth = ParamUtil.nullCheck(request, "fromMonth", home.getFromMonth());
			String fromDay = ParamUtil.nullCheck(request, "fromDay", home.getFromDay());
			String toYear = ParamUtil.nullCheck(request, "toYear", home.getToYear());
			String toMonth = ParamUtil.nullCheck(request, "toMonth", home.getToMonth());
			String toDay = ParamUtil.nullCheck(request, "toDay", home.getToDay());
			int nextPage = ParamUtil.nullCheck(request, "nextPage", home.getCurrentPage());

			home.setCategory(category);
			home.setFromYear(fromYear);
			home.setFromMonth(fromMonth);
			home.setFromDay(fromDay);
			home.setToYear(toYear);
			home.setToMonth(toMonth);
			home.setToDay(toDay);
			home.setCurrentPage(nextPage);
		}
		return home;
	}
}

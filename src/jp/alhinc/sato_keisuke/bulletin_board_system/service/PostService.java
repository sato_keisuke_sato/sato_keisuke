package jp.alhinc.sato_keisuke.bulletin_board_system.service;

import java.sql.Connection;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Post;
import jp.alhinc.sato_keisuke.bulletin_board_system.dao.PostsDao;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.DBUtil;

public class PostService {
	public boolean insertPost (Post post) {
		Connection connection = null;

		try {
			connection = DBUtil.getConnection();
			PostsDao postsDao = new PostsDao();
			int result;
			result = postsDao.insert(connection, post);
			DBUtil.commit(connection);
			if (result > 0) {
				return true;
			} else {
				return false;
			}
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}
}

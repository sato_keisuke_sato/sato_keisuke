package jp.alhinc.sato_keisuke.bulletin_board_system.service;


import java.sql.Connection;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;
import jp.alhinc.sato_keisuke.bulletin_board_system.dao.UsersDao;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.DBUtil;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.EncryptUtil;

public class LoginService {

	public User login(String login_id, String password) {

		Connection connection = null;
		try {

			connection = DBUtil.getConnection();

			UsersDao usersDao = new UsersDao();
			String encPassword = EncryptUtil.encrypt(password);
			User user = usersDao.getUser(connection, login_id, encPassword);
			DBUtil.commit(connection);

			return user;
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}
}
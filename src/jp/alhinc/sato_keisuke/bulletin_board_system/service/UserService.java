package jp.alhinc.sato_keisuke.bulletin_board_system.service;

import java.sql.Connection;
import java.util.ArrayList;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;
import jp.alhinc.sato_keisuke.bulletin_board_system.dao.UsersDao;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.DBUtil;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.EncryptUtil;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.PermissionUtil;

public class UserService {
	/**
	 * 全ユーザー取得
	 * @return
	 */
	public ArrayList<User> getUsers () {
		ArrayList<User> postList = new ArrayList<>();
		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			UsersDao usersDao = new UsersDao();
			postList = usersDao.getUsers(connection);

			DBUtil.commit(connection);

			return postList;
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}

	public boolean signUp (User user) {
		Connection connection = null;

		try {
			connection = DBUtil.getConnection();
			UsersDao usersDao = new UsersDao();
			int result;
			String encPassword = EncryptUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);
			result = usersDao.insert(connection, user);
			DBUtil.commit(connection);

			if (result > 0) {
				return true;
			} else {
				return false;
			}
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}

	public void updateUser (User newUser) {
		Connection connection = null;

		try {
			connection = DBUtil.getConnection();
			UsersDao usersDao = new UsersDao();
			int result;
			if (newUser.getPassword().isEmpty()) {
				result = usersDao.updateWithoutPass(connection, newUser);
			} else {
				String encPassword = EncryptUtil.encrypt(newUser.getPassword());
				newUser.setPassword(encPassword);
				result = usersDao.update(connection, newUser);
			}
			DBUtil.commit(connection);
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}

	/**
	 * ユーザー取得
	 * @param login_id
	 * @return
	 */
	public User getUserWhereId (String id) {
		Connection connection = null;
		try {
			connection = DBUtil.getConnection();
			UsersDao usersDao = new UsersDao();
			User user = usersDao.getUserWhereId(connection, id);
			DBUtil.commit(connection);

			return user;
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}

	/**
	 * 登録するIDがすでに登録済みではないか確認する。
	 * @param login_id
	 * @return true:未登録
	 */
	public boolean isNotUserExist (String login_id) {
		Connection connection = null;
		try {
			connection = DBUtil.getConnection();
			UsersDao usersDao = new UsersDao();
			User user = usersDao.getUserWhereLoginId(connection, login_id);
			boolean result = true;
			if (user != null) {
				result = false;
			}
			DBUtil.commit(connection);

			return result;
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}

	/**
	 * ユーザーの有効性を切り替える
	 * @param user
	 * @return
	 */
	public void changeUserValidity (String id, int validity) {
		Connection connection = null;
		try {
			connection = DBUtil.getConnection();

			UsersDao usersDao = new UsersDao();
			int result = -1;

			if (validity == PermissionUtil.validUser) {
				result = usersDao.userDisable(connection, id);
			} else {
				result = usersDao.userEnable(connection, id);
			}

			DBUtil.commit(connection);

		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}

}

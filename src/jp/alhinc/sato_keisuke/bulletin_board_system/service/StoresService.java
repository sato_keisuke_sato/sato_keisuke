package jp.alhinc.sato_keisuke.bulletin_board_system.service;

import java.sql.Connection;
import java.util.ArrayList;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.Store;
import jp.alhinc.sato_keisuke.bulletin_board_system.dao.StoresDao;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.CloseableUtil;
import jp.alhinc.sato_keisuke.bulletin_board_system.utils.DBUtil;

public class StoresService {
	public ArrayList<Store> getStores () {
		ArrayList<Store> stores = new ArrayList<>();
		Connection connection = null;

		try {
			connection = DBUtil.getConnection();

			StoresDao storesDao = new StoresDao();
			stores = storesDao.getStores(connection);

			DBUtil.commit(connection);

			return stores;
		} catch (RuntimeException e) {
			DBUtil.rollback(connection);
			throw e;
		} catch (Error e) {
			DBUtil.rollback(connection);
			throw e;
		} finally {
			CloseableUtil.close(connection);
		}
	}
}

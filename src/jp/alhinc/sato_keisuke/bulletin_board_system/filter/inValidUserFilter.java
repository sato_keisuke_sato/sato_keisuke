package jp.alhinc.sato_keisuke.bulletin_board_system.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;
import jp.alhinc.sato_keisuke.bulletin_board_system.service.UserService;

@WebFilter(urlPatterns = {"/edit"})
public class inValidUserFilter implements Filter{
	public static final int editPerStore = 1;
	public static final int editPerPosition = 1;

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,ServletException{
    	System.out.println("inValidUserFilter");
        HttpSession session = ((HttpServletRequest)request).getSession();

        UserService service = new UserService();
		String id = request.getParameter("userId");
		User user = service.getUserWhereId(id);

		if (user != null) {
			chain.doFilter(request, response);
		} else {
			HashMap<String, ArrayList<String>> errorMessages = new HashMap<>();
			ArrayList<String> messages = new ArrayList<String>();
            messages.add("※対象のユーザーが見つかりませんでした。");
            errorMessages.put("management", messages);
            session.setAttribute("errorMessages", errorMessages);
            ((HttpServletResponse)response).sendRedirect("management");
		}
	}

    public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}

package jp.alhinc.sato_keisuke.bulletin_board_system.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.alhinc.sato_keisuke.bulletin_board_system.beans.User;

@WebFilter(urlPatterns = {"/index.jsp", "/edit", "/management", "/post", "/signUp"})
public class LoginFilter implements Filter{
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,ServletException{
    	System.out.println("loginFilter");
        HttpSession session = ((HttpServletRequest)request).getSession();
        User user = (User)session.getAttribute("user");
        if(user != null){
            chain.doFilter(request, response);
        }else{
        	HashMap<String, ArrayList<String>> errorMessages = new HashMap<>();
			ArrayList<String> messages = new ArrayList<String>();
            messages.add("※ログインしてください。");
            errorMessages.put("login", messages);
            session.setAttribute("errorMessages", errorMessages);
            ((HttpServletResponse)response).sendRedirect("login");
        }

    }

    public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}

package jp.alhinc.sato_keisuke.bulletin_board_system.beans;

public class Comment {
	private String text;
	private String registered_date;
	private int registered_user;
	private String registered_userName;
	private int post_id;
	private int id;

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getRegistered_date() {
		return registered_date;
	}
	public void setRegistered_date(String registered_date) {
		this.registered_date = registered_date;
	}
	public int getRegistered_user() {
		return registered_user;
	}
	public void setRegistered_user(int registered_user) {
		this.registered_user = registered_user;
	}
	public int getPost_id() {
		return post_id;
	}
	public void setPost_id(int post_id) {
		this.post_id = post_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRegistered_userName() {
		return registered_userName;
	}
	public void setRegistered_userName(String registered_userName) {
		this.registered_userName = registered_userName;
	}
}

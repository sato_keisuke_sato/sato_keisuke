package jp.alhinc.sato_keisuke.bulletin_board_system.beans;

import java.io.Serializable;

public class Post implements Serializable {
    private static final long serialVersionUID = 1L;

	private String subject;
	private String text;
	private String category;
	private String registered_date;
	private int registered_user;
	private String registered_userName;
	private int id;

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getRegistered_date() {
		return registered_date;
	}
	public void setRegistered_date(String registered_date) {
		this.registered_date = registered_date;
	}
	public int getRegistered_user() {
		return registered_user;
	}
	public void setRegistered_user(int registered_user) {
		this.registered_user = registered_user;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRegistered_userName() {
		return registered_userName;
	}
	public void setRegistered_userName(String registered_userName) {
		this.registered_userName = registered_userName;
	}
}

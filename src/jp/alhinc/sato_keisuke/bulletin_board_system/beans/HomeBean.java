package jp.alhinc.sato_keisuke.bulletin_board_system.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import jp.alhinc.sato_keisuke.bulletin_board_system.utils.DateUtil;

public class HomeBean {
	private int currentPage;
	private int allPages;
	private String fromDate;
	private String fromYear;
	private String fromMonth;
	private String fromDay;
	private String toDate;
	private String toYear;
	private String toMonth;
	private String toDay;
	private String category;
	private String comment;
	public static final int dispLimitNum = 5;//1pageに表示する投稿数
	public static final int dispPageNum = 7;//ページ下部に表示するページ数

    public HomeBean() {
    	setCurrentPage(1);
    	setAllPages(1);
    	setFromDate(DateUtil.defaultFromDate);
    	Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.defaultDateFormat);
        sdf.format(cal.getTime());
    	setToDate(sdf.format(cal.getTime()));
    	setCategory("");
    	setComment("");

    	setFromYear("2000");
    	setFromMonth("1");
    	setFromDay("1");

    	sdf = new SimpleDateFormat(DateUtil.defaultYearFormat);
        sdf.format(cal.getTime());
    	setToYear(sdf.format(cal.getTime()));

    	sdf = new SimpleDateFormat(DateUtil.defaultMonthFormat);
        sdf.format(cal.getTime());
    	setToMonth(sdf.format(cal.getTime()));

    	sdf = new SimpleDateFormat(DateUtil.defaultDayFormat);
        sdf.format(cal.getTime());
    	setToDay(sdf.format(cal.getTime()));
    }

	public String getFromYear() {
		return fromYear;
	}

	public void setFromYear(String fromYear) {
		this.fromYear = fromYear;
	}

	public String getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(String fromMonth) {
		this.fromMonth = fromMonth;
	}

	public String getFromDay() {
		return fromDay;
	}

	public void setFromDay(String fromDay) {
		this.fromDay = fromDay;
	}

	public String getToYear() {
		return toYear;
	}

	public void setToYear(String toYear) {
		this.toYear = toYear;
	}

	public String getToMonth() {
		return toMonth;
	}

	public void setToMonth(String toMonth) {
		this.toMonth = toMonth;
	}

	public String getToDay() {
		return toDay;
	}

	public void setToDay(String toDay) {
		this.toDay = toDay;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getAllPages() {
		return allPages;
	}

	public void setAllPages(int allPages) {
		this.allPages = allPages;
	}

	public int getDispLimitNum() {
		return dispLimitNum;
	}

	public int getDispPageNum() {
		return dispPageNum;
	}
}
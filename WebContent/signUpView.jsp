<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー新規登録</title>
		<script>
			function setDisabled() {
				var storeCode = document.getElementById("storesSelect").value;
				var positionCode = document.getElementById("positionsSelect").value;
			    var positions = document.getElementById("positionsSelect");
				var i;

				if (storeCode == 1) {
					positions.options[1].disabled = false;
					positions.options[2].disabled = false;
				} else {
					positions.options[1].disabled = true;
					positions.options[2].disabled = true;
					<% System.out.println("else"); %>
					if ((positionCode == 1) || (positionCode == 2)) {
						<% System.out.println("select"); %>
						positions.options[0].selected = true;
					}
				}
			}
		</script>
	</head>
	<body>
		<div class="main-contents">
			<div id="header">ユーザー新規登録</div>
			<div class="headerSub">
				<p class="subLeft"><img src="img/user.png" width="18" height="18" />　支店：<c:out value="${user.storeName}" />　役職：<c:out value="${user.positionName}" /></p>
				<p class="subRight">　　　<c:out value="${user.name}" /> さん（</p>
				<FORM method="GET" action="logout">
					<button type="submit" class="logout">ログアウト</button>
				</FORM>
				<p class="subRight">）</p>
			</div>
			<hr>
			<div class="menu">
				<ul>
					<li><a href="./">＞ホーム</a></li>
					<li><a href="management">＞ユーザー管理画面</a></li>
				</ul>
			</div>
			<br />
			<div id="formField">
			    <FORM method="POST" action="signUp">
			    	<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
					    	<label for="login_id">ログインID</label><br />
					    	<INPUT type="text" name="login_id" value="${formUser.login_id}">
						</div>
				    	<c:choose>
					    	<c:when test="${ not empty errorMessages.login_id }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.login_id}" var="messageList">
										${messageList}
									</c:forEach>
								</div>
							</c:when>
						</c:choose>
					</div>
					<br />

					<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
							<label for="name">名称</label><br />
							<INPUT type="text" name="name" value="${formUser.name}">
						</div>
						<c:choose>
					    	<c:when test="${ not empty errorMessages.name }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.name}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
						</c:choose>
					</div>
					<br />

					<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
							<label for="store">支店</label><br />
							<select name="store" id="storesSelect" onChange="setDisabled()">
								<c:forEach items="${stores}" var="stores">
									<c:choose>
										<c:when test="${formUser.store == stores.code}">
											<option value="${stores.code}" selected>${stores.name}</option>
										</c:when>
										<c:otherwise>
											<option value="${stores.code}">${stores.name}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<c:choose>
					    	<c:when test="${ not empty errorMessages.store }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.store}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
						</c:choose>
					</div>
					<br />

					<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
							<label for="position">部署・役職</label><br />
							<select name="position" id="positionsSelect" onChange="setDisabled()">
								<c:forEach items="${positions}" var="positions">
									<c:choose>
										<c:when test="${formUser.position == positions.code}">
											<option value="${positions.code}" selected>${positions.name}</option>
										</c:when>
										<c:otherwise>
											<option value="${positions.code}">${positions.name}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<c:choose>
					    	<c:when test="${ not empty errorMessages.position }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.position}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
						</c:choose>
					</div>
					<br />

					<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
							<label for="password">パスワード</label><br />
							<INPUT type="password" name="password">
						</div>
						<c:choose>
					    	<c:when test="${ not empty errorMessages.password }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.password}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
						</c:choose>
					</div>
					<br />

					<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
							<label for="connfirmation">パスワード（確認用）</label><br />
							<INPUT type="password" name="confirmation">
						</div>
						<c:choose>
					    	<c:when test="${ not empty errorMessages.confirmation }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.confirmation}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
						</c:choose>
					</div>
					<br />
					<c:remove var="errorMessages" scope="session" />
					<button type="submit" name="submitSignUp" value="signUp">登録</button>
				</FORM>
			</div>
			<br />
			<hr>
		</div>
	</body>
</html>
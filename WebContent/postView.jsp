<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.beans.*"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
	</head>
	<body>
		<div class="main-contents">
			<div id="header">新規投稿</div>
			<div class="headerSub">
				<p class="subLeft"><img src="img/user.png" width="18" height="18" />　支店：<c:out value="${user.storeName}" />　役職：<c:out value="${user.positionName}" /></p>
				<p class="subRight">　　　<c:out value="${user.name}" /> さん（</p>
				<FORM method="GET" action="logout">
					<button type="submit" class="logout">ログアウト</button>
				</FORM>
				<p class="subRight">）</p>
			</div>
			<hr>
			<div class="menu">
				<ul>
					<li><a href="./">＞ホーム</a></li>
				</ul>
			</div>
			<br />
			<div id="formField">
		    	<FORM method="POST" action="post">
			    	<div class="formFieldItemParent">
				    	<div class="formFieldItemChild">
			    			<label for="line">件名</label><br />
			    			<INPUT type="text" name="subject" style="width:350px;" value="${formPost.subject}"><br />
			    		</div>
				    	<c:if test="${ not empty errorMessages.subject }">
							<div class="errorMessages">
								<c:forEach items="${errorMessages.subject}" var="messageList">
									<c:out value="${messageList}" />
								</c:forEach>
							</div>
						</c:if>
					</div>
					<br />

					<div class="formFieldItemParent">
				    	<div class="formFieldItemChild">
							<label for="line">カテゴリー</label><br />
							<INPUT type="text" name="category" style="width:150px;" value="${formPost.category}"><br />
						</div>
				    	<c:if test="${ not empty errorMessages.category }">
							<div class="errorMessages">
								<c:forEach items="${errorMessages.category}" var="messageList">
									<c:out value="${messageList}" />
								</c:forEach>
							</div>
						</c:if>
					</div>
					<br />

					<div class="formFieldItemParent">
				    	<div class="formFieldItemChild">
							<label for="line">内容</label><br />
							<textarea name="text" style="width:500px;height:200px;"><c:out value="${formPost.text}" /></textarea><br />
						</div>
				    	<c:if test="${ not empty errorMessages.text }">
							<div class="errorMessages">
								<c:forEach items="${errorMessages.text}" var="messageList">
									<c:out value="${messageList}" />
								</c:forEach>
							</div>
						</c:if>
					</div>
					<br />
				<c:remove var="errorMessages" scope="session" />
	  			<button type="submit" name="post" value="投稿">投稿</button>
				</FORM>
			</div>
			<br>
			<hr>
		</div>
	</body>
</html>
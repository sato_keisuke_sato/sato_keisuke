<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.beans.*"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.utils.PermissionUtil"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理</title>
	</head>
	<body>
		<div class="main-contents">
			<div id="header">ユーザー管理</div>
			<div class="headerSub">
				<p class="subLeft"><img src="img/user.png" width="18" height="18" />　支店：<c:out value="${user.storeName}" />　役職：<c:out value="${user.positionName}" /></p>
				<p class="subRight">　　　<c:out value="${user.name}" /> さん（</p>
				<FORM method="GET" action="logout">
					<button type="submit" class="logout">ログアウト</button>
				</FORM>
				<p class="subRight">）</p>
			</div>
			<hr>
			<div class="menu">
				<ul>
					<li><a href="./">＞ホーム</a></li>
					<li><a href="signUp">＞ユーザー新規登録</a></li>
				</ul>
			</div>
			<br />
			<c:choose>
		    	<c:when test="${ not empty errorMessages }">
					<div class="errorMessagesLeft">
						<c:forEach items="${errorMessages.management}" var="messageList">
							<c:out value="${messageList}" />
						</c:forEach>
					</div>
				</c:when>
				<c:otherwise>
				<br />
				</c:otherwise>
			</c:choose>
			<c:remove var="errorMessages" scope="session" />
			<c:if test="${ not empty userList }">
				<div class="userGrid">
					<table border="7" class="userTable">
						<tr>
							<th class="thChara">ログインID</th>
							<th class="thChara">ユーザー名</th>
							<th class="thChara">支店</th>
							<th class="thChara">部署・役職</th>
							<th class="thChara">状態</th>
							<th class="thButton">切替</th>
							<th class="thButton">編集</th>
						</tr>
						<c:forEach items="${userList}" var="userList">
							<c:choose>
								<c:when test="${userList.id == user.id}">
									<label for="bold"><c:set value="-ログイン中-" var="validity" /></label>
								</c:when>
								<c:when test="${userList.validity == 1}">
									<c:set value="-稼働中-" var="validity" />
								</c:when>
								<c:otherwise>
									<c:set value="-停止-" var="validity" />
								</c:otherwise>
							</c:choose>
							<tr>
								<td class="tdChara"><c:out value="${userList.login_id}" /></td>
								<td class="tdChara"><c:out value="${userList.name}" /></td>
								<td class="tdChara"><c:out value="${userList.storeName}" /></td>
								<td class="tdChara"><c:out value="${userList.positionName}" /></td>
								<td class="tdChara"><c:out value="${validity}" /></td>
								<td class="tdButton">
									<FORM method="POST" action="management">
										<INPUT type="hidden" name="changeUserId" value="${userList.id}">
										<INPUT type="hidden" name="changeUserValidity" value="${userList.validity}">

											<c:choose>
												<c:when test="${(userList.validity == 1) && user.id != userList.id}">
													<button type="submit" name="pagename" value="停止" onclick='return confirm("本当に停止しますか？");'>停止</button>
												</c:when>
												<c:when test="${(userList.validity == 1) && user.id == userList.id}">
													<button type="submit"class="ButtonDisabled" disabled>停止</button>
												</c:when>
												<c:otherwise>
													<button type="submit" name="pagename" value="復活" onclick='return confirm("本当に復活しますか？");'>復活</button>
												</c:otherwise>
											</c:choose>

									</FORM>
								</td>
								<td class="tdButton">
									<FORM method="GET" action="edit">
										<INPUT type="hidden" name="userId" value="${userList.id}" >
										<button type="submit" class="manaButton">編集</button>
										<c:remove var="oldUser" scope="session" />
									</FORM>
								</td>
							</tr>
						</c:forEach>
					</table>
					<br>
				</div>
			</c:if>
			<hr>
		</div>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.beans.*"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集</title>
		<script>
			function setDisabled() {
				var storeCode = document.getElementById("storesSelect").value;
				var positionCode = document.getElementById("positionsSelect").value;
			    var positions = document.getElementById("positionsSelect");
				var i;

				if (storeCode == 1) {
					positions.options[1].disabled = false;
					positions.options[2].disabled = false;
				} else {
					positions.options[1].disabled = true;
					positions.options[2].disabled = true;
					<% System.out.println("else"); %>
					if ((positionCode == 1) || (positionCode == 2)) {
						<% System.out.println("select"); %>
						positions.options[0].selected = true;
					}
				}
			}
		</script>
	</head>
	<body>
		<div class="main-contents">
			<div id="header">ユーザー編集</div>
			<div class="headerSub">
				<p class="subLeft"><img src="img/user.png" width="18" height="18" />　支店：<c:out value="${user.storeName}" />　役職：<c:out value="${user.positionName}" /></p>
				<p class="subRight">　　　<c:out value="${user.name}" /> さん（</p>
				<FORM method="GET" action="logout">
					<button type="submit" class="logout">ログアウト</button>
				</FORM>
				<p class="subRight">）</p>
			</div>
			<hr>
			<div class="menu">
				<ul>
					<li><a href="./">＞ホーム</a></li>
					<li><a href="management">＞ユーザー管理画面</a></li>
				</ul>
			</div>
			<br />
			<label for="line">■変更前情報</label><br>
			<c:if test="${ not empty oldUser }">
				<div class="userGrid">
					<table border="2" class="userTable">
						<tr>
							<th>ログインID</th>
							<td class="tdChara">${oldUser.login_id}</td>
						</tr>
						<tr>
							<th>名称</th>
							<td class="tdChara">${oldUser.name}</td>
						</tr>
						<tr>
							<th>支店</th>
							<td class="tdChara">${oldUser.storeName}</td>
						</tr>
						<tr>
							<th>部署・役職</th>
							<td class="tdChara">${oldUser.positionName}</td>
						</tr>
					</table>
				</div>
			</c:if>
			<br />
			<label for="line">■変更後情報</label><br>
			<div id="formField">
				<FORM method="POST" action="edit">
					<INPUT type="hidden" name="userId" value="${oldUser.id}">
					<INPUT type="hidden" name="oldLogin_id" value="${oldUser.login_id}">

					<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
							<label for="line">ログインID</label><br />
							<INPUT type="text" name="newLogin_id" value="${formUser.login_id}">
						</div>
						<c:choose>
							<c:when test="${ not empty errorMessages.newLogin_id }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.newLogin_id}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
						</c:choose>
					</div>
					<br />

					<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
							<label for="line">名称</label><br />
							<INPUT type="text" name="newName" value="${formUser.name}">
						</div>
						<c:choose>
							<c:when test="${ not empty errorMessages.newName }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.newName}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
						</c:choose>
					</div>
					<br />

					<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
							<label for="line">支店</label><br />
							<select name="newStore" id="storesSelect" onChange="setDisabled()">
								<c:forEach items="${stores}" var="stores">
									<c:choose>
										<c:when test="${formUser.store == stores.code}">
											<option value="${stores.code}" selected>${stores.name}</option>
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${user.id == oldUser.id}">
													<option value="${stores.code}" disabled>${stores.name}</option>
												</c:when>
												<c:otherwise>
													<option value="${stores.code}">${stores.name}</option>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<c:choose>
							<c:when test="${ not empty errorMessages.newStore }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.newStore}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
						</c:choose>
					</div>
					<br />

					<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
							<label for="line">部署・役職</label><br />
							<select name="newPosition" id="positionsSelect">
								<c:forEach items="${positions}" var="positions">
									<c:choose>
										<c:when test="${formUser.position == positions.code}">
											<option value="${positions.code}" selected>${positions.name}</option>
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${user.id == oldUser.id}">
													<option value="${positions.code}" disabled>${positions.name}</option>
												</c:when>
												<c:otherwise>
													<option value="${positions.code}">${positions.name}</option>
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
						<c:choose>
							<c:when test="${ not empty errorMessages.newPosition }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.newPosition}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
						</c:choose>
					</div>
					<br />


					<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
							<label for="line">パスワード</label><br />
							<INPUT type="password" name="newPassword">
						</div>
						<c:choose>
							<c:when test="${ not empty errorMessages.newPassword }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.newPassword}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
							<c:otherwise>
								<br />
							</c:otherwise>
						</c:choose>
					</div>
					<br />

					<div class="formFieldItemParent">
			    		<div class="formFieldItemChild">
							<label for="line">パスワード（確認用）</label><br />
							<INPUT type="password" name="confirmation">
						</div>
						<c:choose>
							<c:when test="${ not empty errorMessages.confirmation }">
								<div class="errorMessages">
									<c:forEach items="${errorMessages.confirmation}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
							<c:otherwise>
								<br />
							</c:otherwise>
						</c:choose>
					</div>
					<INPUT type="hidden" name="userId" value="${user.id}">
					<c:remove var="formUser" scope="session" />
					<c:remove var="errorMessages" scope="session" />
					<br />
					<button type="submit" name="signUp" value="登録">登録</button>
				</FORM>
			</div>
			<br />
			<hr>
		</div>
	</body>
</html>

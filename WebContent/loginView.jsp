<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>login</title>
	</head>
	<body>
		<div class="main-contents">
			<div id="header">ログイン</div>
			<hr>
			<br />
			<div id="formField">
				<FORM method="POST" action="login">
					<div class="formFieldItemParent">
				    	<div class="formFieldItemChild">
							<label for="line">ログインID：</label>
							<INPUT type="text" name="login_id" value="${formLogin_id}" style="width:160px;"><br />
							<br />
							<label for="line">パスワード：</label>
							<INPUT type="password" name="password" style="width:160px;"><br />
						</div>
						<c:if test="${ not empty errorMessages.login }">
							<div class="errorMessages">
								<c:forEach items="${errorMessages.login}" var="messageList">
									<label class="errorLabel"><c:out value="${messageList}" /></label>
								</c:forEach>
							</div>
						</c:if>
					</div>
					<br />
					<c:remove var="errorMessages" scope="session" />
					<button type="submit" name="login" value="ログイン">ログイン</button>
					<button type="reset" name="clear" value="クリア">クリア</button>
	    		</FORM>
	    	</div>
    	</div>
	</body>
</html>
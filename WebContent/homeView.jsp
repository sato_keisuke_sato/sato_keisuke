<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.beans.*"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.utils.*"%>
<%@ page import="java.util.ArrayList"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム</title>

		<script>
			function setDays() {
				var optionLoop;
				var fromYear = document.getElementById("fromYear").value;
				var fromMonth = document.getElementById("fromMonth").value;
				var toYear = document.getElementById("toYear").value;
				var toMonth = document.getElementById("toMonth").value;

				optionLoop = function(year, month, id, value) {
					var lastDays = new Array(0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
					if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0){
					  lastDays[2] = 29;
					}
					var lastDay = lastDays[month];
					var opt;
					var i;
					opt = null;
					for (i = 1; i <= lastDay ; i++) {
						if (i === value) {
							opt += "<option value='" + i + "' selected>" + i + "</option>";
						} else {
							opt += "<option value='" + i + "'>" + i + "</option>";
						}
		    		}
		    		return document.getElementById(id).innerHTML = opt;
		  		};
				optionLoop(fromYear, fromMonth, 'fromDay', <%= ((HomeBean)request.getAttribute("home")).getFromDay() %>);
				optionLoop(toYear, toMonth, 'toDay', <%= ((HomeBean)request.getAttribute("home")).getToDay() %>);
			}
		</script>
	</head>
	<body>
		<div class="main-contents">
			<div id="header"><a href="./">ホーム</a></div>
			<div class="headerSub">
				<p class="subLeft"><img src="img/user.png" width="18" height="18" />　支店：<c:out value="${user.storeName}" />　役職：<c:out value="${user.positionName}" /></p>
				<p class="subRight">　　　<c:out value="${user.name}" /> さん（</p>
				<FORM method="GET" action="logout">
					<button type="submit" class="logout">ログアウト</button>
				</FORM>
				<p class="subRight">)</p>
			</div>
			<hr>
			<div class="menu">
				<ul>
					<li><a href="post">＞新規投稿</a></li>
					<li><a href="management">＞ユーザー管理</a></li>
				</ul>
			</div>
			<br />

			<c:if test="${not empty errorMessages.permission}">
				<br />
				<div class="errorMessagesLeft">
					<c:forEach items="${errorMessages.permission}" var="messageList">
						<c:out value="${messageList}" />
					</c:forEach>
				</div>
			</c:if>
			<br />
			<div class="search">
				<FORM method="GET" action="./">
					<label for="category">カテゴリー：<INPUT type="text" name="category" value="${home.category}"></label><br>
					<label for="date">投稿日時：
						<select name="fromYear" id="fromYear">
							<c:forEach begin="2000" end="${home.toYear}" step="1" varStatus="status">
								<c:choose>
									<c:when test="${status.index == home.fromYear}">
										<option value="${status.index}" selected>${status.index}</option>
									</c:when>
									<c:otherwise>
										<option value="${status.index}">${status.index}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>

						<select name="fromMonth" id="fromMonth" onChange="setDays()">
							<c:forEach begin="1" end="12" step="1" varStatus="status">
								<c:choose>
									<c:when test="${status.index == home.fromMonth}">
										<option value="${status.index}" selected>${status.index}</option>
									</c:when>
									<c:otherwise>
										<option value="${status.index}">${status.index}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>

						<select name="fromDay" id="fromDay"></select>
						～
						<select name="toYear" id="toYear">
							<c:forEach begin="2000" end="${home.toYear}" step="1" varStatus="status">
								<c:choose>
									<c:when test="${status.index == home.toYear}">
										<option value="${status.index}" selected>${status.index}</option>
									</c:when>
									<c:otherwise>
										<option value="${status.index}">${status.index}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>

						<select name="toMonth" id="toMonth" onChange="setDays()">
							<c:forEach begin="1" end="12" step="1" varStatus="status">
								<c:choose>
									<c:when test="${status.index == home.toMonth}">
										<option value="${status.index}" selected>${status.index}</option>
									</c:when>
									<c:otherwise>
										<option value="${status.index}">${status.index}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>

						<select name="toDay" id="toDay"></select>
					</label>
					<script>
						setDays();
					</script>
					<INPUT type="hidden" name="nextPage" value="${home.currentPage}">
					<button type="submit" name="submitHome" value="search">検索</button>
				</FORM>
			</div>
			<hr>
			<c:if test="${(home.allPages) == 0}">
				<br />
				<label>条件に合致する投稿は見つかりませんでした。</label>
				<ul>
					<label>検索のヒント:</label>
					<li>キーワードに誤字・脱字がないか確認します。</li>
					<li>別のキーワードを試してみます。</li>
					<li>もっと一般的なキーワードに変えてみます。</li>
				</ul>
			</c:if>
			<div class="posts">
				<c:if test="${ not empty posts }">
					<c:forEach items="${posts}" var="post">
						<div class="postBox">
							<h3><c:out value="${post.subject}" /></h3><br />
							<label for="line">${post.text}</label><br />
							<br />
							<div class="boxSub">
								<label >カテゴリー：<c:out value="${post.category}" /></label><br />
								<label >登録者：<c:out value="${post.registered_userName}" /></label><br />
								<label >登録日時：<c:out value="${post.registered_date}" /></label>
							</div>
							<div class="boxRight">
								<label class="dateDiff">　　-<c:out value="${DateUtil.getDayDiff(post.registered_date)}" />-</label>
								<c:if test="${(post.registered_user).equals(user.id)}">
									<FORM method="POST" action="deletePost" style="display: inline">
										<INPUT type="hidden" name="fromYear" value="${home.fromYear}">
										<INPUT type="hidden" name="fromMonth" value="${home.fromMonth}">
										<INPUT type="hidden" name="fromDay" value="${home.fromDay}">
										<INPUT type="hidden" name="toYear" value="${home.toYear}">
										<INPUT type="hidden" name="toMonth" value="${home.toMonth}">
										<INPUT type="hidden" name="toDay" value="${home.toDay}">
										<INPUT type="hidden" name="category" value="${home.category}">
										<INPUT type="hidden" name="nextPage" value="${home.currentPage}">
										<INPUT type="hidden" name="id" value="${post.id}">
										<button type="submit" name="submitHome" value="deletePost" onclick='return confirm("削除しますか？");'>削除</button>
									</FORM>
								</c:if>
							</div>
						</div>
						<c:if test="${ not empty comments }">
							<c:forEach items="${comments}" var="comment">
								<c:if test="${(comment.post_id).equals(post.id)}">
									<div class="comments">
										<div class="commentBox">
											<label for="bold"><c:out value="${comment.registered_userName}" /></label><br />
											<div class="commentText">
												<label for="line">${comment.text}</label><br />
											</div>
											<br />
											<div class="boxRight">
												<label class="dateDiff">登録日時：<c:out value="${comment.registered_date}" /></label>
												<label class="dateDiff">-<c:out value="${DateUtil.getDayDiff(comment.registered_date)}" />-</label>
												<c:choose>
													<c:when test="${(comment.registered_user).equals(user.id)}">
														<FORM method="POST" action="deleteComment" style="display: inline">
															<INPUT type="hidden" name="id" value="${comment.id}">
															<INPUT type="hidden" name="fromYear" value="${home.fromYear}">
															<INPUT type="hidden" name="fromMonth" value="${home.fromMonth}">
															<INPUT type="hidden" name="fromDay" value="${home.fromDay}">
															<INPUT type="hidden" name="toYear" value="${home.toYear}">
															<INPUT type="hidden" name="toMonth" value="${home.toMonth}">
															<INPUT type="hidden" name="toDay" value="${home.toDay}">
															<INPUT type="hidden" name="category" value="${home.category}">
															<INPUT type="hidden" name="nextPage" value="${home.currentPage}">
															<button type="submit" name="submitHome" value="deleteComment" onclick='return confirm("削除しますか？");'>削除</button>
														</FORM>
													</c:when>
													<c:otherwise>
														<br />
													</c:otherwise>
												</c:choose>
											</div>
										</div>
									</div>
								</c:if>
							</c:forEach>
						</c:if>
						<c:set var="keyString">${post.id}</c:set>
						<c:choose>
							<c:when test="${not empty errorMessages[keyString]}">
								<br />
								<div class="errorMessagesRight">
									<c:forEach items="${errorMessages[keyString]}" var="messageList">
										<c:out value="${messageList}" />
									</c:forEach>
								</div>
							</c:when>
							<c:otherwise>
								<br />
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${post.id == formPost_id}">
								<c:set value="${formComment}" var="textAreaValue" />
							</c:when>
							<c:otherwise>
								<c:set value="" var="textAreaValue" />
							</c:otherwise>
						</c:choose>
						<div class="commentForm">
							<FORM method="POST" action="comment">
								<INPUT type="hidden" name="post_id" value="${post.id}">
								<textarea name="comment" style="width:600px;height:80px;"><c:out value="${textAreaValue}" /></textarea><br>
								<INPUT type="hidden" name="fromYear" value="${home.fromYear}">
								<INPUT type="hidden" name="fromMonth" value="${home.fromMonth}">
								<INPUT type="hidden" name="fromDay" value="${home.fromDay}">
								<INPUT type="hidden" name="toYear" value="${home.toYear}">
								<INPUT type="hidden" name="toMonth" value="${home.toMonth}">
								<INPUT type="hidden" name="toDay" value="${home.toDay}">
								<INPUT type="hidden" name="category" value="${home.category}">
								<INPUT type="hidden" name="nextPage" value="${home.currentPage}">
								<button type="submit" name="submitHome" value="comment">コメント</button>
							</FORM>
						</div>
						<br />
						<hr>
					</c:forEach>
				</c:if>
			</div>
			<c:remove var="errorMessages" scope="session" />
			<c:remove var="formComment" scope="session" />
			<c:remove var="formPost_id" scope="session" />
			<div class="pages">
				<ul>
					<c:if test="${(home.currentPage) > 1 && (home.allPages) > 0}">
						<form method="GET" name="pagingBack" action="./" style="display:inline">
						    <INPUT type="hidden" name="fromYear" value="${home.fromYear}">
							<INPUT type="hidden" name="fromMonth" value="${home.fromMonth}">
							<INPUT type="hidden" name="fromDay" value="${home.fromDay}">
							<INPUT type="hidden" name="toYear" value="${home.toYear}">
							<INPUT type="hidden" name="toMonth" value="${home.toMonth}">
							<INPUT type="hidden" name="toDay" value="${home.toDay}">
							<INPUT type="hidden" name="category" value="${home.category}">
							<INPUT type="hidden" name="nextPage" value="${home.currentPage - 1}">
						</form>
						<li><a href="javascript:document.pagingBack.submit();">前へ</a></li>
					</c:if>

					<c:set value="0" var="page"></c:set>
					<c:set value="${-((home.dispPageNum - 1) / 2)}" var="minuend"></c:set>
					<fmt:formatNumber value="${minuend}" var="minuend" pattern="#####" />
					<c:if test="${(home.currentPage + minuend) > 1}">
						<form method="GET" name="pagingBegin" action="./" style="display:inline">
						    <INPUT type="hidden" name="fromYear" value="${home.fromYear}">
							<INPUT type="hidden" name="fromMonth" value="${home.fromMonth}">
							<INPUT type="hidden" name="fromDay" value="${home.fromDay}">
							<INPUT type="hidden" name="toYear" value="${home.toYear}">
							<INPUT type="hidden" name="toMonth" value="${home.toMonth}">
							<INPUT type="hidden" name="toDay" value="${home.toDay}">
							<INPUT type="hidden" name="category" value="${home.category}">
							<INPUT type="hidden" name="nextPage" value="1">
						</form>
						<li><a href="javascript:document.pagingBegin.submit();">1</a></li>...
					</c:if>
					<c:forEach begin="1" end="${home.dispPageNum}" step="1" varStatus="status">
						<c:set value="${home.currentPage + minuend}" var="page"></c:set>
						<c:if test="${(page > 0) && (home.allPages >= page)}">
							<c:choose>
								<c:when test="${page == home.currentPage}">
									<li><c:out value="${page}"></c:out></li>
								</c:when>
								<c:otherwise>
									<c:set value="paging${page}" var="paging"></c:set>
									<form method="GET" name="${paging}" action="./" style="display:inline">
									    <INPUT type="hidden" name="fromYear" value="${home.fromYear}">
										<INPUT type="hidden" name="fromMonth" value="${home.fromMonth}">
										<INPUT type="hidden" name="fromDay" value="${home.fromDay}">
										<INPUT type="hidden" name="toYear" value="${home.toYear}">
										<INPUT type="hidden" name="toMonth" value="${home.toMonth}">
										<INPUT type="hidden" name="toDay" value="${home.toDay}">
										<INPUT type="hidden" name="category" value="${home.category}">
										<INPUT type="hidden" name="nextPage" value="${page}">
									</form>
									<li><a href="javascript:document.${paging}.submit();">${page}</a></li>
								</c:otherwise>
							</c:choose>
						</c:if>
						<c:set value="${minuend + 1}" var="minuend"></c:set>
					</c:forEach>
					<c:if test="${home.allPages > page}">
						<form method="GET" name="pagingEnd" action="./" style="display:inline">
							<INPUT type="hidden" name="fromYear" value="${home.fromYear}">
							<INPUT type="hidden" name="fromMonth" value="${home.fromMonth}">
							<INPUT type="hidden" name="fromDay" value="${home.fromDay}">
							<INPUT type="hidden" name="toYear" value="${home.toYear}">
							<INPUT type="hidden" name="toMonth" value="${home.toMonth}">
							<INPUT type="hidden" name="toDay" value="${home.toDay}">
							<INPUT type="hidden" name="category" value="${home.category}">
							<INPUT type="hidden" name="nextPage" value="${home.allPages}">
						</form>
						...<li><a href="javascript:document.pagingEnd.submit();">${home.allPages}</a></li>
					</c:if>
					<c:if test="${(home.allPages) > 0}">
					<c:if test="${(home.currentPage) < (home.allPages)}">
						<form method="GET" name="pagingNext" action="./" style="display:inline">
						    <INPUT type="hidden" name="fromYear" value="${home.fromYear}">
							<INPUT type="hidden" name="fromMonth" value="${home.fromMonth}">
							<INPUT type="hidden" name="fromDay" value="${home.fromDay}">
							<INPUT type="hidden" name="toYear" value="${home.toYear}">
							<INPUT type="hidden" name="toMonth" value="${home.toMonth}">
							<INPUT type="hidden" name="toDay" value="${home.toDay}">
							<INPUT type="hidden" name="category" value="${home.category}">
							<INPUT type="hidden" name="nextPage" value="${home.currentPage + 1}">
						</form>
						<li><a href="javascript:document.pagingNext.submit();">次へ</a></li>
					</c:if>
					</c:if>
				</ul>
			</div>
		</div>
	</body>
</html>